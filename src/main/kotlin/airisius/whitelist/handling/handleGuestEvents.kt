package airisius.whitelist.handling

import airisius.whitelist.plugin
import airisius.whitelist.util.isPluginWhitelisted
import io.github.commandertvis.plugin.handle
import io.github.commandertvis.plugin.handleCancellable
import io.github.commandertvis.plugin.worlds
import org.bukkit.GameMode
import org.bukkit.entity.Player
import org.bukkit.event.Cancellable
import org.bukkit.event.entity.*
import org.bukkit.event.player.*
import org.bukkit.plugin.Plugin


private fun Cancellable.processGuestEvent(shouldInformPlayer: Boolean = true) {
	when {
		this is PlayerEvent && !player.isPluginWhitelisted() -> {
			isCancelled = true
			
			if (shouldInformPlayer)
				player.sendMessage(plugin.jsonConfig.messages.guest.cantInteract)
		}
		
		this is EntityDamageByEntityEvent && damager is Player && !entity.isPluginWhitelisted() -> {
			isCancelled = true
			
			if (shouldInformPlayer)
				(damager as Player).sendMessage(plugin.jsonConfig.messages.guest.cantInteract)
		}
		
		this is EntityDamageEvent
				&& entity is Player
				&& (cause == EntityDamageEvent.DamageCause.SUFFOCATION
				|| cause == EntityDamageEvent.DamageCause.DROWNING)
				&& !entity.isPluginWhitelisted() -> {
			
			isCancelled = true
			val location0 = entity.location.clone()
			location0.y += 1
			entity.teleport(location0)
		}
		
		this is EntityEvent && entity is Player && !entity.isPluginWhitelisted() -> {
			isCancelled = true
			
			if (shouldInformPlayer)
				(entity as Player).sendMessage(plugin.jsonConfig.messages.guest.cantInteract)
		}
	}
}

fun Plugin.handleGuestEvents() {
	handle<PlayerJoinEvent> {
		if (!player.isPluginWhitelisted()) {
			if (plugin.jsonConfig.enableGuestMode) {
				player.health = 20.0
				player.foodLevel = 20
				player.saturation = 20.0f
				player.teleport(worlds.first().spawnLocation)
				player.gameMode = GameMode.ADVENTURE
				player.sendMessage(plugin.jsonConfig.messages.guest.join)
			} else
				player.kickPlayer(plugin.jsonConfig.messages.whitelist.format())
		}
	}
	
	handleCancellable<AsyncPlayerChatEvent>(ignoreCancelled = true) { processGuestEvent() }
	handleCancellable<EntityPickupItemEvent>(ignoreCancelled = true) { processGuestEvent(false) }
	handleCancellable<EntityDamageEvent>(ignoreCancelled = true) { processGuestEvent(false) }
	handleCancellable<FoodLevelChangeEvent>(ignoreCancelled = true) { processGuestEvent(false) }
	handleCancellable<PlayerCommandPreprocessEvent>(ignoreCancelled = true) { processGuestEvent() }
	handleCancellable<PlayerInteractEvent>(ignoreCancelled = true) { processGuestEvent() }
	handleCancellable<PlayerStatisticIncrementEvent>(ignoreCancelled = true) { processGuestEvent(false) }
}
