package airisius.whitelist

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*

object Whitelist : Table() {
	private val player = varchar("Player", 36).uniqueIndex()
	
	val players = mutableSetOf<UUID>()
	
	fun updateWhitelist() {
		transaction(plugin.database) {
			players.clear()
			selectAll().forEach { players.add(UUID.fromString(it[player])) }
		}
	}
	
	fun addPlayer(id: UUID): Boolean {
		if(id !in players) {
			transaction(plugin.database) {
				insert { it[player] = id.toString() }
			}
			players.add(id)
			return true
		}
		return false
	}
	
	fun removePlayer(id: UUID): Boolean {
		if(id in players) {
			transaction(plugin.database) {
				deleteWhere { player eq id.toString() }
			}
			players.remove(id)
			return true
		}
		return false
	}
	
	fun check(id: UUID): Boolean {
		return id in players
	}
}
