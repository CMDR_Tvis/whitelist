package airisius.whitelist

import airisius.whitelist.config.Settings
import airisius.whitelist.handling.handleGuestEvents
import io.github.commandertvis.plugin.colorize
import io.github.commandertvis.plugin.command
import io.github.commandertvis.plugin.json.JsonConfigurablePlugin
import io.github.commandertvis.plugin.offlinePlayers
import org.bukkit.Bukkit
import org.bukkit.OfflinePlayer
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

lateinit var plugin: Plugin

class Plugin : JsonConfigurablePlugin<Settings>(Settings()) {
	lateinit var database: Database
	
	override fun onEnable() {
		plugin = this
		initializeWhitelistDatabase()
		registerCommand()
		registerListeners()
	}
	
	private fun initializeWhitelistDatabase() {
		database = Database.connect(
			"jdbc:mysql://${jsonConfig.sql.url}",
			"com.mysql.jdbc.Driver",
			jsonConfig.sql.user,
			jsonConfig.sql.password
		)
		transaction(database) { SchemaUtils.createMissingTablesAndColumns(Whitelist) }
	}
	
	@Suppress("DEPRECATION")
	private fun registerCommand() {
		Whitelist.updateWhitelist()
		Bukkit.getScheduler().runTaskTimer(plugin, { -> Whitelist.updateWhitelist() }, 6000, 6000)
		command(name, setOf("whitelist", "wl")) {
			execution {
				onCommandExecution { sender, _, _ ->
					if(!sender.hasPermission("airisius.whitelist")) {
						sender.sendMessage("&4You are not allowed to run this command".colorize())
						cancel()
					}
				}
				
				"add" subcommand {
					action { _, args ->
						if(Whitelist.addPlayer(Bukkit.getOfflinePlayer(args[1]).uniqueId))
							sendMessage("&4Added player ${args[1]} to whitelist")
						else
							sendMessage("&4Player is already whitelisted")
					}
					
					tabAction { _, _, args ->
						if (args.size <= 1)
							offlinePlayers.filter { !Whitelist.check(it.uniqueId) }.mapNotNull(OfflinePlayer::getName)
						else
							emptyList()
					}
				}
				
				"remove" subcommand {
					action { _, args ->
						if(Whitelist.removePlayer(Bukkit.getOfflinePlayer(args[1]).uniqueId))
							sendMessage("&4Removed player ${args[1]} from whitelist")
						else
							sendMessage("&4Player is not whitelisted")
					}
					
					tabAction { _, _, args ->
						if (args.size <= 1)
							offlinePlayers.filter {Whitelist.check(it.uniqueId)}.mapNotNull(OfflinePlayer::getName)
						else
							emptyList()
					}
				}
				
				"update" subcommand {
					action { _, args ->
						Whitelist.updateWhitelist()
					}
				}
				
				"list" subcommand {
					action { _, args ->
						sendMessage("Whitelisted players:")
						for(player in Whitelist.players) {
							sendMessage("  - ${Bukkit.getOfflinePlayer(player).name}")
						}
					}
				}
				
				default {
					action { _, _ -> }
					tabAction { _, _, _ -> emptyList() }
				}
			}
		}
	}
	
	private fun registerListeners() {
		handleGuestEvents()
	}
}
