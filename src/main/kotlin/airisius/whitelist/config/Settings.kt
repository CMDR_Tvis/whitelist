package airisius.whitelist.config

class Settings {
	val messages = Messages()
	val sql = SQL()
	var enableGuestMode = false
	
	class Messages {
		val guest = Guest()
		var whitelist = "You are not whitelisted on this server."
		
		class Guest {
			var join = "You are playing as a guest."
			var cantInteract = "You are in \"guest\" mode and you can't interact with the world and players."
		}
	}
	
	class SQL {
		var url = ""
		var user = ""
		var password = ""
	}
}
